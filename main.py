from typing import Dict
from typing_extensions import Self
from fastapi import FastAPI, Request, Response, Cookie
from pydantic import BaseModel
import json
import os
import hashlib

app = FastAPI()

posts = []
users = []

data = { "posts": [], "users": [] }


class Post(BaseModel):
    id: str
    description: str
    content: str
    author: str


class User(BaseModel):
    username: str
    password: str
    email: str | None


class Users:
    def __init__(self, username: str = None, email: str = None, salt: str = None, hashed_password: str = None, token: str = None):
        self.username = username
        self.email = email
        self.salt = salt
        self.hashed_password = hashed_password
        self.token = token
    
    def gen_hash(self, password: str):
        """
        Method which genrates a salt and hashes the passwords and stores them in the Users object instance.
        >>> user = Users(email="some@something.com", username="someone")
        >>> user.gen_hash("hello123")
        """
        self.salt = os.urandom(64).hex() # genrate random string and save it as salt
        # Then hash the password
        hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), 100000) # bytes
        self.hashed_password = hash.hex() # get hex value for the hashed password
    
    def dict(self) -> Dict:
        return {
            "username": self.username,
            "email": self.email,
            "salt": self.salt,
            "hashed_password": self.hashed_password,
            "token": self.token
        }

    def from_dict(self, dict_obj: Dict):
        """
        >>> user = Users()
        >>> user.from_dict({'username': 'siddhu' ...})
        """
        self.username = dict_obj["username"]
        self.email =  dict_obj["email"]
        self.salt = dict_obj["salt"]
        self.hashed_password = dict_obj["hashed_password"]
        self.token = dict_obj["token"]
    
    def verify(self, password: str) -> bool:
        """
        >>> user.verify(password)
        >>> print(user.token)
        """
        new_hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), 100000)
        if new_hash.hex() == self.hashed_password:
            self.token = os.urandom(64).hex()
            return True
        else:
            return False

    @classmethod
    def load_from_dict(cls, dict_obj: Dict) -> Self:
        """
        >>> user: Users = Users.load_from_dict({'username': 'siddhu' ...})
        """
        return Users(
            username = dict_obj["username"],
            email = dict_obj["email"],
            salt = dict_obj["salt"],
            hashed_password= dict_obj["hashed_password"],
            token = dict_obj.get("token")
        )


@app.get("/")
def hello(login_token: str | None = Cookie(None)):
    return {
        "message": "Visit '/docs' ",
        "login_token": login_token
    }


@app.get("/test/login")
def test_login(login_token: str | None = Cookie(None)):
    return {
        "message": "Hello",
        "login_token": login_token
    }


@app.post("/post/add")
def add_post(post: Post):
    data.append(post)
    return {
        "message": "Successfully added post to database",
        "post_added": {
            "id": post.id,
            "description": post.description,
            "content": post.content
        }
    }


@app.get("/posts")
def get_all_posts():
    return {
        "results": data
    }


@app.delete("/posts/delete/{post_id}")
def delete_post(post_id: str):
    for entry in data:
        if entry["id"] == post_id:
            data.remove(entry)
            return {
                "message": "Post removed successfully",
                "post": entry
            }
    return {
        "message": "post not found or invalid post_id"
    }


@app.post("/signup")
def signup(user: User):
    for entry in users:
        if entry.username == user.username:
            return {
                "status_code": "0",
                "message": f"User with {user.username} already exists!"
            }   
    new_user = Users(username=user.username, email=user.email)
    new_user.gen_hash(user.password)
    users.append(new_user)
    return {
        "status_code" : "1",
        "message": "user added successfully"
    }


@app.post("/login")
def login(user: User, response: Response):
    for entry in users:
        if entry.username == user.username:
            # check_user = Users.load_from_dict(entry)
            if entry.verify(user.password):
                response.set_cookie(key="login_token", value=entry.token)
                return {
                    "message": "login successful! user authenticated!"
                }
            else:
                return {
                    "message": "wrong password. user verification failed"
                }
    return {
        "message": f"user with {user.username} not found in database"
    }



@app.on_event("startup")
def load_data():
    file_to_load = "data.json"
    if os.path.exists(file_to_load):
        with open(file_to_load, "r") as file:
            global data, users, posts
            data = json.load(file)
            for user in data["users"]:
                users.append(Users.load_from_dict(user))
            posts = data["posts"]



@app.on_event("shutdown")
def save_data():
    with open("data.json", "w") as file:
        users_list = []
        for user in users:
            users_list.append(user.dict())
        posts_list = []
        for post in posts:
            posts_list.append(post.dict())
        data["posts"] = posts_list
        data["users"] = users_list
        json.dump(data, file, indent=4)